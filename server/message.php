<?php
    include('connect.php');
    include('session.php');

    setSession();
    

        if(isset($_POST['addQuestion']))
        {
            if(isset($_POST['content']) && !empty($_POST['content']) && isset($_POST['register']) && !empty($_POST['register']))
            {
                $question = $_POST['content'];
                $matricule = $_POST['register'];
                date_default_timezone_set('Europe/Moscow');
                $dateRegistered =  date('Y-m-d H:i:s');

                $ajoutQuestion = $con->prepare("INSERT INTO question(question,Matricule,dateEnvoi) VALUES (?,?,?)");
                
                $ajoutQuestion-> bind_param("sss",$question,$matricule,$dateRegistered);
                $ajoutQuestion->execute();
                $ajoutQuestion->close();

                $con-> close();
                
            }
        }

        
        
        function displayTopic($conn)//function for displaying topics published by the logged user 
        {
            $req = 'SELECT question from question WHERE Matricule ='.$_SESSION['register'];
            $res = mysqli_query($conn,$req);
            $row = mysqli_fetch_array($res);
            return $row;//returns all of the topics in an array
        }



        

        


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form action="" method="post">
      
        <input type="text" name="content" autofocus="autofocus" value = "<?= displayTopic($con)?>">
        <input type="text" name="register" readonly = "readonly" value="<?= $_SESSION['register'];?>"/>
        <input type="submit" value="Envoyer" name = "addQuestion">
        <input type="submit" value="Reponses" name = "response">
    </form>
</body>
</html>