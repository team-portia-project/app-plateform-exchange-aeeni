<?php
    
    include("connect.php");
    include('session.php');
    
    setSession();

    if(isset($_POST['submit']))
    {
        //recuperation
        $register = $_POST['matricule'];
        $username = $_POST['username'];
        $email = $_POST['email'];
        $password = $_POST['password'];
        $confirm = $_POST['confirm'];
        

        //condition
        if(!empty($register) && !empty($username) && !empty($email) && !empty($password) && !empty($confirm))
        {
            //Preparation
            $stmt = $con->prepare("INSERT INTO signup(Matricule,username,email,password,confirmPassword) VALUES(?,?,?,?,?)");

            $checkDuplicata = "SELECT *FROM signup WHERE Matricule = '$register'";
            $res = mysqli_query($con,$checkDuplicata);
            $row = mysqli_fetch_array($res);

                if($password==$confirm)
                {
                    $password = password_hash($password,PASSWORD_BCRYPT);
                    $confirm = password_hash($confirm,PASSWORD_BCRYPT);
                    
                    if ($row['Matricule'] == $register)
                    {
                        return false;
                    } 
                    else
                    {
                        $stmt->bind_param("sssss",$register,$username,$email,$password,$confirm);
                        $stmt->execute();
                        $stmt->close();

                        $_SESSION['register'] = $register;
                        
                        header('Location:../clients/home.php');
                    } 
                        
                }
                else
                {
                    echo "Reessayer";
                }
                $con->close();
            
        }
        else
        {
            echo "Veuillez completer les champs";
        }
    }
  
?>