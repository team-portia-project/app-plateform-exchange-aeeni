<?php
    
    function setSession() : void
    {
        if(!(session_id()))
        {
            session_start();
            session_regenerate_id(true);
        }
        
    }

    function stopSession() : void
    {
        session_unset();
        session_destroy();
    }
?>