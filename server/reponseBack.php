<?php
    include('connect.php');
    // include('rechercheBack.php');
    if(isset($_POST['repondre']))
    {
        $idQues = 3;
        $commentaire = $_POST['commentaire'];
        $dateRepond = date('Y/m/d  H:i');
        
        $req = $con->prepare("INSERT INTO reponse (idQues, commentaire, dateRepond) values(?,?,?)");
        $req->bind_param("sss",$idQues,$commentaire,$dateRepond);
        $req->execute();
        $req->close();
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form action="" method="post">
        <input type="text" name="commentaire" id="" placeholder="Tapez votre reponse">
        <input type="submit" name="repondre" value="Repondre">
    </form>
</body>
</html>