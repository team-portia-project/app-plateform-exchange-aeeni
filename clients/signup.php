<?php
include('../server/signUp.php');
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Sign in</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="uicons-solid-rounded/css/uicons-solid-rounded.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" media="screen" href="styleLog.css" />
</head>
<body>
    <div class="box-signin">
        <div class="form-signin">
        <form action="signUp.php" method="post">
            <h2>Sign in</h2>
                <div class="inputBox">
                    <input type="text" required ="required"  name = "email">
                    <span><i class="fi-sr-mailbox"></i>Email</span>
                    <i class="line"></i>
                </div>
                <div class="inputBox">
                    <input type="text" required ="required" name = "username">
                    <span><i class="fi-sr-user"></i>Username</span>
                    <i class="line"></i>
                </div>
                <div class="inputBox">
                    <input type="text" required ="required" name = "matricule">
                    <span><i class="fi-sr-laptop-code"></i>Student ID</span>
                    <i class="line"></i>
                </div>
                
                <div class="inputBox">
                    <input type="password" required ="required" name = "password">
                    <span><i class="fi-sr-key"></i>Password</span>
                    <i class="line"></i>
                </div>
                <div class="inputBox">
                    <input type="password" required ="required" name = "confirm">
                    <span><i class="fi-sr-badge-check"></i>Confirm password</span>
                    <i class="line"></i>
                </div>
                    <input type="submit" value="Sign in" name = "submit">
            </form>
        </div>
    </div>    
</body>
</html>
