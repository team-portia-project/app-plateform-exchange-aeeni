<?php
    include('../server/loginBack.php');
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Log in</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="uicons-solid-rounded/css/uicons-solid-rounded.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" media="screen" href="styleLog.css" />
</head>
<body>
    <div class="box">
        <form action="../server/loginBack.php" method="post">
            <div class="form">
                <h2>Log in</h2>
                <div class="inputBox">
                    <input type="text" required ="required" name="register">
                    <span><i class="fi-sr-mailbox"></i>N° Matricule</span>
                    <i class="line"></i>
                </div>
                <div class="inputBox">
                    <input type="password" required ="required" name="password">
                    <span><i class="fi-sr-key"></i>Password</span>
                    <i class="line"></i>
                </div>
                <div class="links">
                    <a href="#">Forgot password?</a>
                    <a href="#" class="signup">Signup</a>
                </div>
                <input type="submit" name="submit" value="Log in">
            </div>
        </form>
    </div>    
</body>
</html>
