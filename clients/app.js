function shareQuestion(){
    var content = document.querySelector('#content');
    var p = document.createElement('p');
    var text = document.createTextNode(document.querySelector('textarea').value);

    p.id = "post";
    p.appendChild(text);
    
    var photo = document.createElement('img');
    photo.src = "images/pdp.jpg";
    photo.id = "emoticone";

    var name = document.createElement('strong');
    var nameText = document.createTextNode('Miantsa Iarilanja Ramahaliarivo');
    name.appendChild(nameText);

    var btn = document.createElement('button');
    btn.appendChild(document.createTextNode('Code'));
    btn.id = "code";

    var showCode = document.createElement('div');
    showCode.id = "showCode";
    btn.addEventListener('click',function(){
        showCode.appendChild(document.createTextNode("Code Html, css, js"));
        showCode.style.clear = "right";
        content.insertBefore(showCode,btn2);
    })

    var btn2 = btn.cloneNode(false);
    btn2.appendChild(document.createTextNode('Follow'));
    btn2.id = "follow"; 

    var btn3 = btn.cloneNode(false);
    btn3.appendChild(document.createTextNode('Answers'));
    btn3.id = "answer";
    btn.class = "accordion";

    content.appendChild(photo);
    content.appendChild(name);
    content.appendChild(p);
    content.appendChild(btn);
    content.appendChild(btn2);
    content.appendChild(btn3);

}